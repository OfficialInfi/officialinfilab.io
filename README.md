# infinixius.gitlab.io

This repository holds the frontend and backend for my [website](https://officialinfi.gitlab.io), hosted as a gitlab.io domain. The [legacy website](http://infinixius.co.nf), will continue to stay up to redirect users to the new website.


## Links

* [Contributing Guidelines](https://officialinfi.gitlab.io/pages/ProjectContributing)

* [Code of Conduct](https://officialinfi.gitlab.io/pages/ProjectCodeofConduct)


## Support

* [Discord Server](https://discord.gg/p4xEr5d)

* [Subreddit](https://www.reddit.com/r/unknowncommunity)


## Acknowledgements

You can view the people who have influenced this project [here](https://gitlab.com/Infinixius/infinixius.gitlab.io/wikis/Acknowledgements). You can also view people who have contributed to the repository [here](https://gitlab.com/Infinixius/infinixius.gitlab.io/-/graphs/master).

This project is licensed under the [MIT License](https://officialinfi.gitlab.io/pages/ProjectLicense).